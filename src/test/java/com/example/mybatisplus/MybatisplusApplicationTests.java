package com.example.mybatisplus;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.mybatisplus.mapper.AdminMapper;
import com.example.mybatisplus.model.domain.Admin;
import com.example.mybatisplus.service.AdminService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Wrapper;

@SpringBootTest//   相当于启动主方法
class MybatisplusApplicationTests {
    @Autowired
    AdminService adminService;
    @Autowired//操作admin表，mapper操作
    AdminMapper adminMapper;
    @Test
    public void qq(){
        Admin admin =new Admin();
        admin.setLoginName("zhangsan");
        admin.setPassword("123456");
       adminMapper.insert(admin);
       //删除 条件构造器
        QueryWrapper<Admin>wrapper=new QueryWrapper<>();
        wrapper.eq("1", 1).or().eq("LoginName", "lxp");
        adminMapper.delete(wrapper);
        Admin admin1 =new Admin().setId(1l).setLoginName("hello");
        adminMapper.updateById(admin1);

    }


}
